default["macprovisioner"]["apps"] = {
  "dmg": {
    "Sonos": "http://update.sonos.com/software/mac/mdcr/SonosDesktopController100.dmg", # accept_eula true
    "Steam": "https://steamcdn-a.akamaihd.net/client/installer/steam.dmg", # accept_eula true
    "Transmission": "https://github.com/transmission/transmission-releases/raw/master/Transmission-2.94.dmg",
    "Origin": "https://origin-a.akamaihd.net/Origin-Client-Download/origin/mac/live/Origin.dmg",
    "Battle.net": "https://eu.battle.net/download/getInstaller?os=mac&installer=Battle.net-Setup.dmg",
    "Kindle": "https://s3.amazonaws.com/kindleformac/52077/KindleForMac-52077.dmg",
    "MEGASync": "https://mega.nz/MEGAsyncSetup.dmg",
    "Google Chrome": "https://dl-ssl.google.com/chrome/mac/stable/GGRM/googlechrome.dmg",
    "Slack": "https://slack.com/ssb/download-osx",
    "Skype": "https://download.skype.com/s4l/download/mac/Skype-8.54.0.91.dmg",
    "Disk Inventory X": "http://www.derlien.com/diskinventoryx/downloads/dev/DIX1.0Universal.dmg",
    "Android File Transfer": "https://dl.google.com/dl/androidjumper/mtp/current/androidfiletransfer.dmg",
    "Tunnelblick": "https://tunnelblick.net/release/Tunnelblick_3.7.8_build_5180.dmg",
    # "X-Lite": "http://counterpath.s3.amazonaws.com/downloads/X-Lite_5.4.0_94385.dmg", # accept_eula true
    "GIMP-2.10": "https://download.gimp.org/mirror/pub/gimp/v2.10/osx/gimp-2.10.10-x86_64.dmg",
    "Firefox": "https://download-installer.cdn.mozilla.net/pub/firefox/releases/65.0/mac/en-US/Firefox%2065.0.dmg",
    "GeforceNOW": "https://download.nvidia.com/gfnpc/GeForceNOW-release.dmg", # App name is actually NVIDIA Geforce NOW
    # "Crosslink": "", # Mac App Store only
    # "Microsoft Remote Desktop": "", # Mac App Store only
    # "Keka": "http://download.kekaosx.com/", # doesn't offer anything useful
    # "GoToMeeting": "",
    # "MPlayerX": "",
    # "Wunderlist": "", # Mac App Store only
  },
  "dmg_pkg": {
    # "TeamViewer": "https://dl.tvcdn.de/download/TeamViewer.dmg",
    "RDM": "http://avi.alkalay.net/software/RDM/RDM-2.2.dmg",
    # "VMware AirWatch Agent": "https://awagent.com/Home/DownloadMacOsxAgentApplication",
    # "VirtualBox": "http://download.virtualbox.org/virtualbox/5.2.2/VirtualBox-5.2.2-119230-OSX.dmg",
  },
  "zip": {
    "NFOViewer": "http://blockart.sourceforge.net/NFOViewer%20(0.3%20beta%203).zip",
    "Atom": "https://atom-installer.github.com/v1.34.0/atom-mac.zip?s=1546643669&ext=.zip",
    "KeepingYouAwake": "https://github.com/newmarcel/KeepingYouAwake/releases/download/1.5.0/KeepingYouAwake-1.5.0.zip",
    "OpenEmu": "https://github.com/OpenEmu/OpenEmu/releases/download/v2.0.8/OpenEmu_2.0.8.zip",
  },
  "pkg": {
    "GOG Galaxy": "https://content-system.gog.com/open_link/download?path=/open/galaxy/client/galaxy_client_1.2.56.20.pkg",
    # "Skype for Business": "https://download.microsoft.com/download/D/0/5/D055DA17-C7B8-4257-89A1-78E7BBE3833F/skypeforbusinessinstaller-16.24.0.191.pkg"
  }
}
