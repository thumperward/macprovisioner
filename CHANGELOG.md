# `macprovisioner` CHANGELOG

-   0.1.1 (7 June 2019)
    -   Chris Cunningham: bump version after 18 months of work (oops)
    -   Chris Cunningham: fix GIMP and GOG Galaxy
-   0.1.0 (26 November 2017)
    -   Chris Cunningham: initial commit
