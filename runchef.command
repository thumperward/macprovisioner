#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR"

if [ ! $(which chef-client) ]; then
  curl -L https://omnitruck.chef.io/install.sh | sudo bash -s -- -v 1.6.11 -P chefdk
fi
mkdir nodes
berks vendor cookbooks
sudo chef-client -z -o macprovisioner
rm -rf cookbooks
sudo rm nodes/*
