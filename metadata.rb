name              'macprovisioner' # ~FC078
maintainer        'Chris Cunningham'
maintainer_email  'chris.cunningham@cloudreach.com'
license           'All Rights Reserved'
description       'Installs/Configures macprovisioner'
long_description  'Installs/Configures macprovisioner'
version           '0.2.0'
chef_version      '>= 14.10.9' if respond_to?(:chef_version)
source_url        'https://bitbucket.org/chriscunningham/macprovisioner/src'
issues_url        'https://bitbucket.org/chriscunningham/macprovisioner/issues'

supports          'mac_os_x'

depends           'zip_app'
depends           'pkg'
