
Current base image from <https://app.vagrantup.com/AndrewDryga/boxes/vagrant-box-osx>


Inspiration:

-   <https://rocu.de/how-to-setup-your-mac-automatically-with-chef.html>
-   <https://github.com/peteclark-ft/setup>
-   <http://jtimberman.housepub.org/blog/2012/07/29/os-x-workstation-management-with-chef/>
-   <https://github.com/chef-osx/macapps/>
-   <https://github.com/pje/dotfiles/blob/master/osx.sh>
-   <https://github.com/pauldambra/chef-zero-laptop-setup>


<https://github.com/carsomyr/chef-plist/tree/master/vendor/cookbooks/plist>

<http://krypted.com/mac-os-x/adding-objects-to-the-dock/>
<https://gist.github.com/rettuce/71c801881e1433c3a9de>

```xml
<dict>
  <key>tile-data</key>
  <dict>
    <key>file-data</key>
    <dict>
      <key>_CFURLString</key>
      <string>/Applications</string>
      <key>_CFURLStringType</key>
      <integer>0</integer>
    </dict>
    <key>file-label</key>
    <string>Apps</string>
    <key>file-type</key>
    <integer>18</integer>
  </dict>
  <key>tile-type</key>
  <string>directory-tile</string>
</dict>
```

defaults delete com.apple.dock - reset it

the mac_os_x_userdefaults helper doesn't really work
hypothesis is that it's quoting booleans

...huh. no. it seems to accept it on the terminal no matter what. so why not in
Chef?

is it setting it globally?

okay, I deleted the vagrant user's pref, and this is still a thing. wat. TK
converges, and if the value hasn't changed it keeps it, but the desktop says the pref doesn't
exist at all (as I deleted it). So what am I setting in Chef?

... it's setting it as root I think. Yep. Okay, need to set it as the current user.

... yep, working now. So need to figure out how to set it for everyone. is that possible?

so local vs global is ~/Library/Preferences and /Network/Library/Preferences

so no, isn't possible. never mind. will need to specify the current user for every one

so for mouse direction: the resource doesn't notice if it's been changed, and changing it
with defaults doesn't work even if restarting Finder. doing it with the prefs panel updates
it immediately. so something's broken again.

... this pref is just ignoring the user anyway. WHAT.

booo. `log ENV['USER']` shows root. so what we'll need to do is iterate over all
the user accounts. Argh.

`dscl . list /Users | grep -v '_'` - also includes some system accounts

just grep -v daemon, nobody, root

.....ah, system user templates

/System/Library/User Template/Non_localized/Library/Preferences/.GlobalPreferences.plist

look into that later

for now, wat, can't set these by user? for net writes okay, for mouse not okay

so yet again, this is garbage basically. just a toy. do it the hard way

AH, HERE IS EDITING THE defaults

<https://apple.stackexchange.com/questions/28730/how-to-i-set-the-scroll-direction-to-be-non-natural-for-any-new-users-created>

```
    sudo defaults write /System/Library/User\ Template/Non_localized/Library/Preferences/.GlobalPreferences  com.apple.swipescrolldirection -bool false
```

reboot is the only way to make this change apparently - wat


ooooh

<https://github.com/RoboticCheese/mac-app-store-chef> - ah, needs credentials stored. nope


so how do we do this in chef zero

a script that downloads chefdk, installs git, clones it and runs?

need to uninstall git from the vm first and test

okay, cool. xcode-select?

okay, accounted for - but _something_ really wants to run gcc early in the Chef
run. Chef itself? Apparently - even happens with all dependencies and recipes
commented out

ignoring that for now

okay, wrote a provisioning script. fails with `-W` on my laptop during zip
installs, but let's see if I can get it to run remotely



so doing this remotely...


kitchen create
log in and install chefdk

```bash
curl -L https://omnitruck.chef.io/install.sh | sudo bash -s -- -v 12.18.31
git clone https://chriscunningham@bitbucket.org/chriscunningham/macprovisioner
cd macprovisioner
mkdir nodes
sudo /opt/chef/embedded/bin/gem install berkshelf
/opt/chef/embedded/bin/berks vendor cookbooks
./runchef.sh
```

(yes, sudo and the opt path for berks for some reason)


YEEEEES WORKED

there's an issue with running with -W locally. wonder if that works remotely

seems to - whyrun isn't supported by zip_app_package. maybe it also doesn't support 13?

retrying with chef 13

maybe `curl -L https://omnitruck.chef.io/install.sh | sudo bash -s -- -v 12.18.31 -P chefdk` to get the other bits

... doesn't work with 12.18.31. hmmm

okay, no -v with no -P gives no kitchen or berks. trying no -v with -P

YES. so let's see if this all works. try -W first again

-W fails, so does no -W. Thinking it's chef 13

okay, we can disable those, but let's see if 12 works

... yup, just broken with chef 13. joooooy

okay, disable those for now. but real progress!

so running Chef cookbooks locally is actually an absolute piece of piss

Assuming you have ChefDK installed:

1.  Download a cookbook
2.  `cd cookbookname`
3.  `mkdir nodes`
4.  `berks vendor cookbooks`
5.  `sudo chef-client -z -o cookbookname`

gonna try this on my windows machine. psycho.

OOOOH

~/Library/Application Support/Google/Chrome/Default/Bookmarks

-   this is JSON

<https://github.com/dhoer/chef-chrome> also exists for Chrome but might be
overkill now we have bookmarks working

sod it - going to make my own boxes. You can set an arbitrary URL, right? so
ultimately this is the best solution

doing a windows 10 box using the 10 Pro key from my Dell


here's how to do it manually: <http://huestones.co.uk/node/305>

I'm gonna end up doing this with packer on the mac, I bloody know it

in fact I can surely nearly bloody do it already. Does CSL-C-MI work on Win
through Test Kitchen? Let's check

no idea if this has ever run before. omg what am I doing


<https://github.com/boxcutter/macos>


I should create myself a fixed High Sierra install image



so for when I'm doing the mac vagrant, I can edit the vagrantfile to forward
VNC to a different port - wonder if this is done by default

so my current yosemite doesn't do any forwarding. I think that Windows one was
good, in the end. yeah, it was. haven't looked at the config but I bet it's
good



OKAY I HAVE AN IMAGE - the rebuild script needed some sleeps??

okay, I need packer - which is distributed as a fucking zipped executable, wtf

copied it to /opt/chefdk/bin instead of messing with $PATH

so - ten past three in the morning, but I has a box! wot I do wiv it

vagrant box add box/virtualbox/macos1012-1.0.1.box --name mac-os-x-sierra


OKAY MAC WORKS YAY

every 30 seconds or so it freezes for 5, dunno why, but otherwise it's all good

one other issue was it didn't seem to be headless? will need to check why

<https://www.vagrantup.com/docs/virtualbox/configuration.html>

argh - maybe need to adjust

let's try the naive approach of just removing the v.gui directive

Yeh. fucking mint. Why would that not just be the default now


okayyyyyy - moved VirtualBox VM dir onto external disk and all my woes are gone

sierra freezing:
<https://github.com/timsutton/osx-vm-templates/issues/43> (launchd?)
<https://github.com/boxcutter/macos/issues/19> (two CPUs?)
try number 2 first

YAY, two CPUs is the fix! add this to my vagrantfile

next - case goodies in osx recipe by version - can I get this from ohai plz

YES. fix in.

future things: AppleScript! AppleScript can do all the things.

I should do two big things:

1.  move resolution switcher to main recipe
2.  enable it and push res up - this is a big feature

should also enable SSH

there we go

/Applications/RDM.app/Contents/MacOS/SetResX --modes


I should do a mac store one over the weekend

I want to enable remote desktop, and then I want to box it up with a forwarded
port

do this one first as it's a big win

demo video:

run a full converge including all the goodies

first show the desktop without any customisation - add captions later with
iMovie

then run the script and show a bit of the converge

ooooh... maybe I could then pan out to show it was all being done in a VM?

Intel-only issue couldn't be fixed on VirtualBox as of last year:

<http://www.ulduzsoft.com/2015/11/mac-os-x-guest-and-virtualbox-5-on-amd-cpu/>

here's an AppleScript approach to wallpaper setting:

```
    osascript -e ‘tell application “Finder” to set desktop picture to POSIX file “/path/to/picture.jpg”’
```

AH! here we go!

<https://apple.stackexchange.com/questions/40644/how-do-i-change-desktop-background-with-a-terminal-command>

```
    sqlite3 ~/Library/Application\ Support/Dock/desktoppicture.db "update data set value = '/path/to/any/picture.png'";
    killall Dock;
```

and OH GOSH LOOK AT THIS

<https://github.com/rgcr/m-cli>

this will probably be a big help

OOH THIS IS USEFUL

<https://stackoverflow.com/questions/5125907/how-to-run-a-shell-script-in-os-x-by-double-clicking>

COCOASUDO

<https://github.com/performantdesign/cocoasudo/blob/master/build/Release/cocoasudo>

./cocoasudo chef-client -z -o macprovisioner

let's do this now, why not

another set of useful prefs: <https://gist.github.com/andrewvaughan/8923161>


okay, time to get back on this

1.  add unit tests
2.  pick an appropriate set of defaults, organise like on Windows
3.  get desktop background working
4.  increase default res with rdm command line (maybe not in vbox) - /Applications/RDM.app/Contents/MacOS/SetResX --modes



lol, so Sonos now fails to install because the EULA is too long! you can't just
echo Y into hdiutil because you need to page through the EULA. let's see if there's
an update

... no, I lie. it also passes in PAGER=true - which should fix that. dunno why
this is broken :(

oh, the other EULA ones are broken too. great?

maybe it's this image? oh no


and now it fails on the old one. what the actual fuck

maybe berks? I'm checking in lock files now

please let it be the recent (3 months) change to the dmg cookbook


nope, no idea

BUT THERE IS A THING

X-LITE WORKS

is that recipe somehow breaking the others

no. for some reason hdiutil doesn't see a eula though the gui does

going to try to debug these failing two


....wait. new version of OSX perhaps? try with yosemite again?


anyway, other things to add:

<https://github.com/kristovatlas/osx-config-check> - various tweaks available
<https://pipenv.readthedocs.io/en/latest/> - sane python version management


soooo...new fun. Virtualbox tries to install a system extension, and High Sierra
blocks this. Or at least wants confirmation. Can I automate this? or do we need
to disable

unsigned kexts no longer loaded by default: <https://support.symantec.com/en_US/article.TECH247516.html>

maybe I can pref that first??

<https://datajar.co.uk/macos-high-sierra-secure-kernel-extension-loading/>

can be disabled but only from recovery mode etc. ffs.
`spctl kext-consent`

so just allow this to fail on 10.13 or else disable it. le sigh.

... oh, still no tap to click? sort that

... seems to very between operating systems

<https://github.com/mathiasbynens/dotfiles/pull/612/files?diff=split>

I can probably get the OS version from the command line, and decide based on
that? do it in the attributes file, and make all my resources conditional on it

the latter works for El Capitan as well. let's see if we can figure out the
bare minimum required. difficult through virtualbox unfortunately

```
defaults write com.apple.AppleMultitouchTrackpad Clicking -int 1
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
```

1 is definitely required - maybe the others aren't? can try unsetting them


...


so, chef 14. Lync is broken. but seemingly not by anything stupid

the dmg resource uses hdituil to check for a password
<https://github.com/chef/chef/blob/a447245e468f25c76d40e6c644280aa45e55784c/lib/chef/resource/dmg_package.rb#L160>

hdiutil imageinfo, however, craps out

Properties:
 Encrypted: false
 Kernel Compatible: true
 Checksummed: true
 Software License Agreement: false
 Partitioned: false
 Compressed: true
Resize limits (per hdiutil resize -limits):
STDERR: hdiutil: resize: cannot get resize information for "/tmp/kitchen/cache/Microsoft Lync.dmg"
(failed - internal error)
hdiutil: imageinfo failed - internal error
---- End output of /usr/bin/hdiutil imageinfo  '/tmp/kitchen/cache/Microsoft Lync.dmg' ----
Ran /usr/bin/hdiutil imageinfo  '/tmp/kitchen/cache/Microsoft Lync.dmg' returned 1

this probably needs to be worked around


with `hdiutil resize -limits '/tmp/kitchen/cache/Microsoft Lync.dmg' -debug`:

```
CUDIFDiskImage::setBackingStore calling generateGlobalBLKXFromBLKXTable
CUDIFEncoding::closeResourceFile
flattened
CUDIFDiskImage::setBackingStore returning 0
DIDiskImageNewWithBackingStore: instantiator returned 0
DIDiskImageNewWithBackingStore: returning 0
copyResizeInformation2: entry with flags 0x00000003, inMaxSectorsAvailable 0 resizeInfoHint 0x0
copyResizeInformation2: DIMediaKitCreateMKMediaRef returned 0
CZlibDecompressor::decompressData: src 0x7FE0A4D07B50 srcLen 19
CZlibDecompressor::decompressData: dest 0x7FE0A4D07950 destLen 512
CZlibDecompressor::decompressData: src 0x10A1E0EC5 srcLen 315
CZlibDecompressor::decompressData: dest 0x10A1A0000 destLen 32256
CZlibDecompressor::decompressData: src 0x10A1E0481 srcLen 2943
CZlibDecompressor::decompressData: dest 0x10A1A0000 destLen 262144
CZlibDecompressor::decompressData: src 0x7FE0A4D06ED0 srcLen 19
CZlibDecompressor::decompressData: dest 0x7FFEE5B9C1B0 destLen 512
CZlibDecompressor::decompressData: src 0x10A1E0EC5 srcLen 315
CZlibDecompressor::decompressData: dest 0x10A1A0000 destLen 32256
copyResizeInformation2: MKCFReadMedia returned 0
Schemes:
	0:
		ID: ISO9660
		Name: ISO 9660 CD Partition Scheme
		Sections:
			0:
				Offset: 64
				Overhead: 64
				Partitions:
					0:
						Offset: 64
						Name: MICROSOFTLYNC
						GUID: 270A8BBE-0434-4DDA-93EB-C437E285B337
						Type: PRIMARY VOLUME DESCRIPTOR
						Size: 82440
					1:
						Offset: 64
						Type: SECONDARY VOLUME DESCRIPTOR
						GUID: F4992FA0-AFB9-4671-99C1-3974D93250AD
						Size: 82440
				_Reference: 0x006881A5E07F0000
				Media Block Count: 82504
				Section Block Count: 82504
				Media Offset: 64
				ID: MAP
				Media Block Size: 2048
	1:
		ID: APM
		Name: Apple Partition Scheme
		Sections:
			0:
				_Reference: 0x004281A5E07F0000
				Name: Apple
				ID: MAP
				Overhead: 1
				GUID: B7688078-955C-4F11-BBAC-7A9C8D0BF128
				Media Block Count: 3174563839
				Offset: 1
				Partition ID: 1
				Status: 16777216
				Size: 63
				Media Offset: 1
				Section Block Count: 3174563840
				Type: Apple_partition_map
				Media Block Size: 2048
				Partitions:
					0:
						Offset: 1416
						Status: 1090519159
						Size: 80060
						Type: Apple_HFS
						GUID: 9D8931E1-23C1-4E3E-916A-F0736894D6A4
						Name: DiscRecording 6.0.4d1
						Partition ID: 2
DIMKHybridLayoutIsWritable: ISO partition 0 offset 64 size 82440
DIMKHybridLayoutIsWritable: ISO partition 1 offset 64 size 82440
DIMKHybridLayoutIsWritable: APM partition 0 offset 1416 size 80060
DIMKHybridLayoutIsWritable: ISO data conflicts with APM data
copyResizeInformation2: _preanalyzeScan returned 45
copyResizeInformation2: returning:
(null)
CUDIFEncoding::~CUDIFEncoding
CBSDBackingStore::closeDataFork fSIZE = 40860762
returned from DIHLCopyResizeInfo with 999
hdiutil: resize: failed. internal error (999)
```

okay, the second-last line returns one result (!)

<https://justus.berlin/2014/01/fix-for-time-machine-backup-problem-dihldiskimageattach-returned-999/>

default-mac-os-x-high-sierra:~ root# hdiutil chpass /tmp/kitchen/cache/Microsoft\ Lync.dmg
hdiutil: chpass failed - Function not implemented

nope, lol

okay, putting this down to a broken image

... okay, let's try rebuilding that image this week, but for the time being:

... FIX PYTHON

1.  install pyenv from the pyenv-installer project
2.  add 3.6, 3.7, 2.7 (system 2.7 is fucking broken)
3.  might need to first do a workaround to fix xcode includes
4.  set 2.7 as global


fwiw I fixed pip by updating it and pipenv (with --ignore-installed) and then
uninstalling it. probably my bad

```
VBoxManage modifyvm macinbox --cpuidset 00000001 000306a9 04100800 7fbae3ff bfebfbff
VBoxManage setextradata macinbox VBoxInternal/Devices/efi/0/Config/DmiSystemProduct MacBookPro11,3
VBoxManage setextradata macinbox VBoxInternal/Devices/efi/0/Config/DmiSystemVersion 1.0
VBoxManage setextradata macinbox VBoxInternal/Devices/efi/0/Config/DmiBoardProduct Mac-2BD1B31983FE1663
VBoxManage setextradata macinbox VBoxInternal/Devices/smc/0/Config/DeviceKey ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc
VBoxManage setextradata macinbox VBoxInternal/Devices/smc/0/Config/GetKeyFromRealSMC 1
```

need to add this to all the VMs

High Sierra seems to be good so if we can let's just copy all its config

the problem is the Vagrantfile - tbh I can probably just handwrite this

```
          Task.run %W[ VBoxManage setextradata macinbox GUI/ScaleFactor 2.0 ] + [task_opts] if @hidpi
```

that's all the retina option does. why is mojave affected but not catalina?

ahhhh

`<ExtraDataItem name="VBoxInternal2/EfiGraphicsResolution" value="2160x1440"/>`
vs
`<ExtraDataItem name="VBoxInternal2/EfiGraphicsResolution" value="1280x800"/>`

easily fixed too. hangover from change to macinbox

so:

-   port back: resolution
-   port forward: OS updates
-   both: SMC key bypass, general unification of vagrantfile approach


sierra and yosemite have a big bunch of stuff

```
v.customize ["modifyvm", :id, "--audiocontroller", "hda"]
v.customize ["modifyvm", :id, "--cpus", "2"]
v.customize ["modifyvm", :id, "--boot1", "dvd"]
v.customize ["modifyvm", :id, "--boot2", "disk"]
v.customize ["modifyvm", :id, "--chipset", "ich9"]
v.customize ["modifyvm", :id, "--firmware", "efi"]
v.customize ["modifyvm", :id, "--hpet", "on"]
v.customize ["modifyvm", :id, "--keyboard", "usb"]
v.customize ["modifyvm", :id, "--memory", "2048"]
v.customize ["modifyvm", :id, "--mouse", "usbtablet"]
v.customize ["modifyvm", :id, "--vram", "128"]
```

let's just ditch it, and also this from the newer ones:

```
config.vm.provider "virtualbox" do |v|
  v.gui = true
end
```

that leaves:

```ruby
Vagrant.configure(2) do |config|
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
end
```

in box.ovf let's add the needful:

```xml
<ovf:Info>Complete VirtualBox machine configuration in VirtualBox format</ovf:Info>
<ExtraData>
  <ExtraDataItem name="CustomVideoMode1" value="1280x800x32"/>
  <ExtraDataItem name="VBoxInternal/Devices/efi/0/Config/DmiBoardProduct" value="Mac-2BD1B31983FE1663"/>
  <ExtraDataItem name="VBoxInternal/Devices/efi/0/Config/DmiSystemProduct" value="MacBookPro11,3"/>
  <ExtraDataItem name="VBoxInternal/Devices/efi/0/Config/DmiSystemVersion" value="1.0"/>
  <ExtraDataItem name="VBoxInternal/Devices/smc/0/Config/DeviceKey" value="ourhardworkbythesewordsguardedpleasedontsteal(c)AppleComputerInc"/>
  <ExtraDataItem name="VBoxInternal/Devices/smc/0/Config/GetKeyFromRealSMC" value="1"/>
  <ExtraDataItem name="VBoxInternal2/EfiGraphicsResolution" value="1280x800"/>
</ExtraData>
```

okay, we go again

guru meditation in yosemite, boo. but it was headless! maybe the `Dmi` values
above need to be tweaked

aaahhh this is still missing from some

```xml
<Hardware>
  <CPU count="2">
    <PAE enabled="true"/>
    <LongMode enabled="true"/>
    <HardwareVirtExLargePages enabled="true"/>
    <CpuIdTree>
      <CpuIdLeaf id="1" eax="198313" ebx="68159488" ecx="2142954495" edx="3219913727"/>
    </CpuIdTree>
  </CPU>
  ...
```

bet that's it

even without that, mojave and high sierra live

hmmm. catalina paused itself though

that's fixed yosemite
... and sierra!

... lol spoke too soon, pauses at desktop??

gonna stop fiddling the files and just fix them with vboxmanage maybe - but I
need to fix the files eventually. maybe we're just running out of ram?

... hypothesis looking good!

maybe we can just remove the vagrantfiles entirely tbh. perhaps they need to
exist but can be empty

my high sierra doesn't autologin. probably needs a rebuild unfortunately. OH
but was this the manual one? YES. so grab the old one off E:

also grabbing the 10.15 - let's diff them

would be nice to get keyboard autodetected while we're at it

... and then cata failed, but pretty sure it's because my SSD is full

... right, awesome possum. retest 13 and 15 and then bake them all

So 13.8.5 is the last version available for macos1010. testing if we still work
on that. probably not, booo
