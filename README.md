# macprovisioner README

This cookbook is intended to contain everything needed to set up a new
Cloudreach CSD for work on a fresh laptop.

It could also serve as the future base for a centrally-managed profile.

## Usage

This is designed to be used by copying the directory onto the new machine and
then running `runchef.sh`, which will bootstrap the process.

The basic process is:

1.  Copy the directory to the machine to be provisioned. (Alternatively, one
    could use git to clone it, but this would require xcode to be manually
    installed first - this should be one-click on modern MacOS, but still)
2.  Double-click `runchef.command` - this will ask for a sudo password.
3.  There's no step 3. There's no step 3!

The `runchef.command` script does the following:

1.  Installs ChefDK
2.  Creates a `nodes` directory
3.  Uses Berkshelf to locally vendor all of the dependencies
4.  Invokes Chef Zero to run the cookbook
5.  Tidies up after itself

## Recipes

The `default` recipe invokes the main cookbook recipes. At present this
includes everything. In future this should be set to a sane subset for
general work, with special recipes there if required.

### Main

The recipes are as follows:

-   `development`: installs XCode command-line tools (e.g. Git)
-   `packages`: installs a variety of useful packages used throughout
    Cloudreach (Chrome, Atom, GoToMeeting, Slack) along with a variety of
    recreational packages that are useful for Cloudreachers (Sonos, Steam)
-   `preferences`: adjusts some basic system preferences to make using macOS
    less painful (desktop icons, mouse options, Finder layout etc)

## Attributes

The cookbook uses attributes to control the set of packages that are installed.

## Testing

The cookbook comes with a Test Kitchen configuration which uses Vagrant to boot
an OS X image provided in the Vagrant boxes repository.

Note that this requires quite a lot of disk space.

## Issues / future work

-   Verify that there is full test coverage
-   Inspect for deprecation warnings or things that could be improved by using
    native Chef features in newer versions of Chef

## License and author

-   Chris Cunningham <mailto:chris.cunningham@cloudreach.com>
-   All rights reserved - open to a public release after skunkworks
