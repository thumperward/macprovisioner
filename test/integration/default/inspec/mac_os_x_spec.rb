# frozen_string_literal: true

# Cookbook Name::       kitchen_testing
# Description::         Test export of the node information to chef_node.json
# Recipe::              default_spec
# Author::              Chris Cunningham
#
# Copyright 2019, Chris Cunningham
#
# All rights reserved - Do Not Redistribute
#

# Docker doesn't set TMP
tmpdir = os_env('TMP').content || '/tmp'
node_file = 'kitchen/cache/chef_node.json'
node_path = File.join(tmpdir, node_file)
node = json(node_path).params

describe os[:family] do
  it { should eq 'darwin' }
end

describe command('xcode-select -p') do
  its('exit_status') { should eq 0 }
  # /Applications/Xcode.app/Contents/Developer
  # or /Library/Developer/CommandLineTools
  its('stdout') { should cmp %r{\/Developer} }
  its('stderr') { should eq '' }
end

node['macprovisioner']['apps'].each_value do |app_list|

  app_list.each_key do |app_name|
    describe directory("/Applications/#{app_name}.app") do
      it { should exist }
    end

    # inspec currently only supports checking homebrew packages on macos
    # describe package(app_name) do
    #   it { should be_installed }
    # end
  end
end
