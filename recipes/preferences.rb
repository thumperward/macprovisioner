#
# Cookbook:: macprovisioner
# Recipe:: preferences
#
# Copyright:: 2019, Chris Cunningham, All Rights Reserved.

# Remove any autologin user

execute 'disable-autologin' do
  command 'defaults delete /library/Preferences/com.apple.loginwindow autoLoginUser'
  only_if 'defaults read /library/Preferences/com.apple.loginwindow autoLoginUser'
end

# List user accounts
cliout = Mixlib::ShellOut.new("dscl . list /Users | grep -v '_' | egrep -v 'daemon|nobody|root|puppet'")
cliout.run_command
users = cliout.stdout.split("\n")

# Set account preferences for each user
users.each do |useraccount|
  # Add the Applications directory to the dock
  # This could be cleaner, if I could figure out https://github.com/carsomyr/chef-plist/tree/master/vendor/cookbooks/plist
  execute "#{useraccount}-add-apps-to-dock" do
    user useraccount
    command 'defaults write com.apple.dock persistent-others -array-add "<dict><key>tile-data</key><dict><key>displayas</key><integer>1</integer><key>file-data</key><dict><key>_CFURLString</key><string>/Applications</string><key>_CFURLStringType</key><integer>0</integer></dict><key>file-label</key><string>Apps</string><key>file-type</key><integer>18</integer></dict><key>tile-type</key><string>directory-tile</string></dict>"'
    not_if "defaults read /Users/#{useraccount}/Library/Preferences/com.apple.dock | grep \"\\\"file-label\\\" = App\""
    notifies :run, 'execute[killall Dock]', :delayed
  end

  # Enable dock zoom
  execute "#{useraccount}-enable-dock-zoom" do
    user useraccount
    command 'defaults write com.apple.dock magnification -boolean true'
    not_if 'defaults read com.apple.dock magnification | grep 1'
    notifies :run, 'execute[killall Dock]', :delayed
  end

  execute "#{useraccount}-dock zoom size" do
    user useraccount
    command 'defaults write com.apple.dock largesize -int 90'
    not_if 'defaults read com.apple.dock largesize | grep 90'
    notifies :run, 'execute[killall Dock]', :delayed
  end

  # Fix scrolling direction, because this isn't a phone
  # Note: doesn't work immediately, but sets the right pref: might need a restart?
  execute "#{useraccount}-fix-scrolling" do
    user useraccount
    command 'defaults write NSGlobalDomain com.apple.swipescrolldirection -boolean false'
    not_if 'defaults read NSGlobalDomain com.apple.swipescrolldirection | grep 0'
  end

  # Turn on scrollbars, because we're not sadists
  execute "#{useraccount}-enable-scrollbars-always" do
    user useraccount
    command 'defaults write NSGlobalDomain AppleShowScrollBars -string Always'
    not_if 'defaults read NSGlobalDomain AppleShowScrollBars | grep Always'
    notifies :run, 'execute[killall Dock]', :delayed
    notifies :run, 'execute[killall Finder]', :delayed
  end

  # Enable second click
  execute "#{useraccount}-enable-second-click" do
    user useraccount
    command 'defaults write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -boolean true'
    not_if 'defaults read NSGlobalDomain com.apple.trackpad.enableSecondaryClick | grep 1'
  end

  # Disable network .DS_Store scribbling
  execute "#{useraccount}-kill-net-dsstore" do
    user useraccount
    command 'defaults write com.apple.desktopservices DSDontWriteNetworkStores -boolean true'
    not_if 'defaults read com.apple.desktopservices DSDontWriteNetworkStores | grep 1'
    notifies :run, 'execute[killall Finder]', :delayed
  end

  # Enable desktop icons
  %w(
    ShowExternalHardDrivesOnDesktop
    ShowHardDrivesOnDesktop
    ShowRemovableMediaOnDesktop
    ShowMountedServersOnDesktop
  ).each do |mount|
    execute "#{useraccount}-#{mount}" do
      user useraccount
      command "defaults write com.apple.finder #{mount} -boolean true"
      not_if "defaults read com.apple.finder #{mount} | grep 1"
      notifies :run, 'execute[killall Finder]', :delayed
    end
  end

  # Chrome bookmarks
  directory "/Users/#{useraccount}/Library/Application Support/Google" do
    owner useraccount
  end

  directory "/Users/#{useraccount}/Library/Application Support/Google/Chrome" do
    owner useraccount
  end

  directory "/Users/#{useraccount}/Library/Application Support/Google/Chrome/Default" do
    owner useraccount
  end

  cookbook_file "/Users/#{useraccount}/Library/Application Support/Google/Chrome/Default/Bookmarks" do
    source 'Bookmarks.json'
    owner useraccount
  end

  # Desktop background

  #cookbook_file "#{Chef::Config['file_cache_path']}/cr-desktop.png" do
  #  source 'cr-desktop.png'
  #nd

  #execute "#{useraccount}-background" do
  #  command "sqlite3 \"~/Library/Application\ Support/Dock/desktoppicture.db\" \"update data set value = \'#{Chef::Config['file_cache_path']}/cr-desktop.png\';\""
  #  notifies :run, 'execute[killall Finder]', :delayed
  #end

  # Hide dotfiles
  execute "#{useraccount}-hide-dotfiles" do
    user useraccount
    command 'defaults write com.apple.finder AppleShowAllFiles -boolean false'
    not_if 'defaults read com.apple.finder AppleShowAllFiles | grep 0'
    notifies :run, 'execute[killall Finder]', :delayed
  end

  # Set default Finder view to arrange by kind
  # execute 'sort-by-kind1' do
  #   user useraccount
  #   command 'defaults write com.apple.Finder FXPreferredViewStyle icnv'
  #   not_if 'defaults read com.apple.Finder FXPreferredViewStyle | grep icnv'
  #   notifies :run, 'execute[sort-by-kind2]'
  # end
  # execute 'sort-by-kind2' do
  #   user useraccount
  #   command 'defaults write com.apple.Finder FXPreferredGroupBy kind'
  #   action :nothing
  #   notifies :run, 'execute[sort-by-kind3]'
  # end
  # execute 'sort-by-kind3' do
  #   user useraccount
  #   command '/usr/libexec/PlistBuddy -c "Set :StandardViewSettings:IconViewSettings:arrangeBy kind" ~/Library/Preferences/com.apple.finder.plist'
  #   action :nothing
  #   notifies :run, 'execute[kill-existing-dsstores]', :delayed
  # end
end

# Set default Finder view settings
execute 'kill-existing-dsstores' do
  command 'find /Users -name .DS_Store -delete'
  action :nothing
  notifies :run, 'execute[killall Finder]', :delayed
end

# Restart dock to enable preference changes

execute 'killall Dock' do
  action :nothing
  only_if 'ps -ef | grep Dock | grep -v grep'
end

# Restart finder to enable preference changes

execute 'killall Finder' do
  action :nothing
  only_if 'ps -ef | grep Finder | grep -v grep'
end
