#
# Cookbook:: macprovisioner
# Recipe:: development
#
# Copyright:: 2019, Chris Cunningham, All Rights Reserved.

# Install command-line tools from xcode
# https://github.com/timsutton/osx-vm-templates/blob/ce8df8a7468faa7c5312444ece1b977c1b2f77a4/scripts/xcode-cli-tools.sh
# Note: may not work if system has pending updates
# development may be broken on new macos releases
bash 'install-xcode-tools' do
  live_stream true
  code <<-EOH
    touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress
    PROD=$(softwareupdate -l | grep "\*.*Command Line" | awk -F"*" '{print $2}' | sed -e 's/^ *//' -e '/^$/d' | tail -1 | tr -d '\n')
    softwareupdate -i "$PROD"
  EOH
  not_if 'xcode-select -p'
end

# execute "install SDK headers" do
#   command "installer -pkg /Library/Developer/CommandLineTools/Packages/macOS_SDK_headers_for_macOS_10.14.pkg -target /"
#   not_if { ::File.directory?("/var/include") }
# end

remote_file "#{Chef::Config['file_cache_path']}/pyenv.sh" do
  source "https://pyenv.run"
  mode 0755
end

execute "install pyenv" do
  user ENV["SUDO_USER"]
  environment lazy {({
    "HOME" => ENV["HOME"],
    "SUDO_USER" => ENV["SUDO_USER"],
  })}
  command "#{Chef::Config["file_cache_path"]}/pyenv.sh"
  live_stream true
  not_if { File.directory?("#{ENV['HOME']}/.pyenv") }
end

cookbook_file "#{ENV['HOME']}/.bash_profile" do
  source ".bash_profile"
end

# %w{2.7.16 3.6.8}.each do |py_version|
#   execute "install Python #{py_version}" do
#     # need to do as a user
#     user ENV["SUDO_USER"]
#     environment lazy {({
#       "HOME" => ENV["HOME"],
#       "SUDO_USER" => ENV["SUDO_USER"],
#     })}
#     # command "source .bash_profile && pyenv install #{py_version}"
#     command "pyenv install #{py_version}"
#     cwd ENV["HOME"]
#     live_stream true
#     not_if "#{ENV['HOME']}/.pyenv/bin/pyenv versions | grep #{py_version}"
#   end
# end
#
# # %w{3.7.2}.each do |py_version|
# #   execute "install Python #{py_version}" do
# #     # need to do as a user
# #     user ENV["SUDO_USER"]
# #     environment lazy {({
# #       "HOME" => ENV["HOME"],
# #       "SUDO_USER" => ENV["SUDO_USER"],
# #     })}
# #     command "source .bash_profile && CFLAGS='-I$(xcrun --show-sdk-path)/usr/include' pyenv install #{py_version}"
# #     cwd ENV["HOME"]
# #     live_stream true
# #     not_if "#{ENV['HOME']}/.pyenv/bin/pyenv versions | grep #{py_version}"
# #   end
# # end
#
# execute "set default python" do
#   user ENV["SUDO_USER"]
#   environment lazy {({
#     "HOME" => ENV["HOME"],
#     "SUDO_USER" => ENV["SUDO_USER"],
#   })}
#   cwd ENV["HOME"]
#   command "source .bash_profile && pyenv global 2.7.16"
#   live_stream true
#   not_if "#{ENV['HOME']}/.pyenv/bin/pyenv global | grep 2.7.16"
# end

# execute 'install pip' do
#   command 'easy_install pip'
#   not_if { File.exist?("/usr/local/bin/pip") }
# end
