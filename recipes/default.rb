#
# Cookbook:: macprovisioner
# Recipe:: default
#
# Copyright:: 2019, Chris Cunningham, All Rights Reserved.

include_recipe 'macprovisioner::development'
include_recipe 'macprovisioner::packages'
include_recipe 'macprovisioner::preferences'
