#
# Cookbook:: macprovisioner
# Recipe:: packages
#
# Copyright:: 2019, Chris Cunningham, All Rights Reserved.

node["macprovisioner"]["apps"].each do |type, app_list|
  case type
  when "dmg"
    app_list.each do |app_name, app_source|
      dmg_package app_name do
        source app_source
        accept_eula true
      end
    end

  when "dmg_pkg"
    app_list.each do |app_name, app_source|
      dmg_package app_name do
        source app_source
        type "pkg"
      end
    end

  when "zip"
    app_list.each.each do |app_name, app_source|
      zip_app_package app_name do
        source app_source
      end
    end

  when "pkg"
    app_list.each do |app_name, app_source|
      pkg_package app_name do
        source app_source
      end
    end
  end
end

# Spotify comes as an installer which then has to be run
# zip_app_package 'Spotify' do
#   source 'https://download.scdn.co/SpotifyInstaller.zip'
#   destination Chef::Config['file_cache_path']
#   not_if { ::File.directory?("#{Chef::Config['file_cache_path']}/Install Spotify.app") }
# end

# execute 'install Spotify' do
#   command "open \"#{Chef::Config['file_cache_path']}/Install Spotify.app/Contents/MacOS/Install Spotify\""
#   not_if { ::File.directory?('/Applications/Spotify.app') }
#   ignore_failure true #  error 10810 - installer runs anyway
#   notifies :delete, "directory[#{Chef::Config['file_cache_path']}/Install Spotify.app]", :delayed
# end

# execute 'kill Spotify' do
#   command 'killall Spotify'
#   only_if 'ps -ef | grep Spotify | grep -v grep'
#   action :nothing
# end

# directory "#{Chef::Config['file_cache_path']}/Install Spotify.app" do
#   action :nothing
#   recursive true
#   notifies :run, 'execute[kill Spotify]', :before
# end
